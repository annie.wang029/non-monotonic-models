import torch
from networks.fixup_resnet import fixup_resnet32
import os, sys
sys.path.append(os.getcwd())

import torch
import numpy as np
import torch.backends.cudnn as cudnn
import pynvml

import itertools
from os.path import join as pathjoin
from copy import deepcopy
from collections import OrderedDict


def prepare_net(net, use_gpu=True):

    handle = None
    device = 'cpu'
    if not use_gpu:
        print('Running on CPUs')
        return net, device, handle
    
    if torch.cuda.is_available():
        device = 'cuda'

    if device != 'cpu':
        print('Running on GPU')
        net = net.to(device)
        # net = torch.nn.DataParallel(net)
        cudnn.benchmark = True
        pynvml.nvmlInit()
        handle = pynvml.nvmlDeviceGetHandleByIndex(0)
        print(pynvml.nvmlDeviceGetName(handle))
    else:
        print('No CUDA devices available, run on CPUs')
    
    return net, device, handle


net = fixup_resnet32()
model = torch.load('resnet32-lr0_01-CIFAR10-noBN-Adam/run_1/init.pth')

net, device, handle = prepare_net(net, use_gpu=True)

new_state_dict = OrderedDict()
for k, v in model.items():
    name = k[7:] # remove `module.`
    new_state_dict[name] = v

# load params
net.load_state_dict(new_state_dict)